const $ = arg => document.querySelector(arg);

const modal = $("#modal");
const span = $(".close");

const buttons = document.querySelectorAll(".buy");

buttons.forEach(button => {
    button.addEventListener('click', () => {
        modal.style.display = "block";
    });
})



span.addEventListener('click', () => {
    modal.style.display = "none";
});

window.addEventListener('click', event => {
    if (event.target === modal) {
        modal.style.display = "none";
    }
});

